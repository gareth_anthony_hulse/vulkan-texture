/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef APP_VULKAN_HPP
#define APP_VULKAN_HPP

#define GLFW_INCLUDE_NONE
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <cstdio>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <set>
#include <cstring>
#include <optional>
#include <cstdint>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <array>
#include <chrono>
#include <thread>

namespace app
{
  class vulkan
  {
  public:
    // Functions:
    ~vulkan ();
    
    void run ();

  private:
    // Structs:
    struct queue_family_indices
    {
      std::optional<uint32_t> graphics_family;
      std::optional<uint32_t> present_family;

      bool is_complete ();
    };

    struct swap_chain_support_details
    {
      VkSurfaceCapabilitiesKHR capabilities;
      std::vector<VkSurfaceFormatKHR> formats;
      std::vector<VkPresentModeKHR> present_modes;
    };

    struct vertex
    {
      glm::vec3 pos;
      glm::vec3 color;
      glm::vec2 tex_coord;

      static VkVertexInputBindingDescription get_binding_description ();
      static std::array<VkVertexInputAttributeDescription, 3> get_attribute_descriptions ();
    };

    struct uniform_buffer_object
    {
      alignas (16) glm::mat4 model;
      alignas (16) glm::mat4 view;
      alignas (16) glm::mat4 proj;
    };
    
    // Variables:
#ifndef NDEBUG
    const std::vector<const char*>
    _M_validation_layers =
      {
       "VK_LAYER_KHRONOS_validation"
      };
    
    VkDebugUtilsMessengerEXT _M_debug_messenger;
#endif // NDEBUG
    
    const std::vector<const char*>
    _M_device_extensions =
      {
       VK_KHR_SWAPCHAIN_EXTENSION_NAME
      };
    
    const std::vector<app::vulkan::vertex>
    _M_vertices =
      {
       /* {{x, y, z}, \\ pos.
	*  {red, green, blue}, \\ color.
	*  {x, y}} \\ tex_coord
	*/

       {{-0.5f, -0.5f, 0.0f},
	{1.0f, 0.0f, 0.0f},
	{1.0f, 0.0f}},
       
       {{0.5f, -0.5f, 0.0f},
	{0.0f, 1.0f, 0.0f},
	{0.0f, 0.0f}},
       
       {{0.5f, 0.5f, 0.0f},
	{0.0f, 0.0f, 1.0f},
	{0.0f, 1.0f}},

       {{-0.5f, 0.5f, 0.0f},
	{1.0f, 1.0f, 1.0f},
	{1.0f, 1.0f}},
       
       {{-0.5f, -0.5f, -0.5f},
	{1.0f, 0.0f, 0.0f},
	{1.0f, 0.0f}},
       
       {{0.5f, -0.5f, -0.5f},
	{0.0f, 1.0f, 0.0f},
	{0.0f, 0.0f}},
       
       {{0.5f, 0.5f, -0.5f},
	{0.0f, 0.0f, 1.0f},
	{0.0f, 1.0f}},

       {{-0.5f, 0.5f, -0.5f},
	{1.0f, 1.0f, 1.0f},
	{1.0f, 1.0f}}
      };

    const std::vector<uint16_t>
    _M_indicies =
      {
       0, 1, 2, 2, 3, 0,
       4, 5, 6, 6, 7, 4
      };
    
    GLFWwindow* _M_window;
    VkInstance _M_instance;
    VkPhysicalDevice _M_physical_device = VK_NULL_HANDLE;
    VkDevice _M_logical_device;
    VkQueue _M_graphics_queue;
    VkQueue _M_present_queue;
    VkSurfaceKHR _M_surface;
    const std::string _M_target_session_type = "wayland";
    VkSwapchainKHR _M_swap_chain;
    std::vector<VkImage> _M_swap_chain_images;
    VkFormat _M_swap_chain_image_format;
    VkExtent2D _M_swap_chain_extent;
    std::vector<VkImageView> _M_swap_chain_image_views;
    VkRenderPass _M_render_pass;
    VkPipelineLayout _M_pipeline_layout;
    VkPipeline _M_graphics_pipeline;
    std::vector<VkFramebuffer> _M_swap_chain_framebuffers;
    VkCommandPool _M_command_pool;
    std::vector<VkCommandBuffer> _M_command_buffers;
    std::vector<VkSemaphore> _M_image_available_semaphores;
    std::vector<VkSemaphore> _M_render_finished_semaphores;
    std::vector<VkFence> _M_in_flight_fences;
    std::vector<VkFence> _M_images_in_flight;
    const uint _M_max_frames_in_flight = 2;
    size_t _M_current_frame = 0;
    bool _M_framebuffer_resized = false;
    VkBuffer _M_vertex_buffer;
    VkDeviceMemory _M_vertex_buffer_memory;
    VkBuffer _M_index_buffer;
    VkDeviceMemory _M_index_buffer_memory;
    VkDescriptorSetLayout _M_descriptor_set_layout;
    std::vector<VkBuffer> _M_uniform_buffers;
    std::vector<VkDeviceMemory> _M_uniform_buffers_memory;
    VkDescriptorPool _M_descriptor_pool;
    std::vector<VkDescriptorSet> _M_descriptor_sets;
    VkImage _M_texture_image;
    VkDeviceMemory _M_texture_image_memory;
    VkImageView _M_texture_image_view;
    VkSampler _M_texture_sampler;
    std::chrono::duration<double, std::milli> _M_delta_ms;
    VkImage _M_depth_image;
    VkDeviceMemory _M_depth_image_memory;
    VkImageView _M_depth_image_view;
    
    // Functions:
#ifndef NDEBUG
    bool _M_check_validation_layer_support ();
    static VKAPI_ATTR VkBool32 VKAPI_CALL
    _M_debug_callback (VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
		       VkDebugUtilsMessageTypeFlagsEXT message_type,
		       const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
		       void* user_data);
    void _M_setup_debug_messenger ();
    VkResult _M_create_debug_messenger (VkInstance instance,
					const VkDebugUtilsMessengerCreateInfoEXT* create_info,
					const VkAllocationCallbacks* allocator,
					VkDebugUtilsMessengerEXT* debug_messenger);
    void _M_destroy_debug_messenger (VkInstance instance,
				     VkDebugUtilsMessengerEXT debug_messenger,
				     const VkAllocationCallbacks* allocator);
    void _M_populate_debug_messenger_create_info (VkDebugUtilsMessengerCreateInfoEXT& create_info);
#endif //NDEBUG
    
    void _M_init_window ();
    void _M_init_vulkan ();
    void _M_main_loop ();

    void _M_create_instance ();
    std::vector<const char*> _M_get_required_instance_exts ();
    void _M_pick_physical_device ();
    bool _M_is_physical_device_suitable (VkPhysicalDevice physical_device);
    queue_family_indices _M_find_queue_families (VkPhysicalDevice physical_device);
    void _M_create_logical_device ();
    void _M_create_surface ();
    bool _M_check_device_extension_support (VkPhysicalDevice physical_device);
    swap_chain_support_details _M_query_swap_chain_support (VkPhysicalDevice physical_device);
    VkSurfaceFormatKHR _M_choose_swap_surface_format (const std::vector<VkSurfaceFormatKHR>&
						      available_formats);
    VkPresentModeKHR _M_choose_swap_present_mode (const std::vector<VkPresentModeKHR>&
						  available_present_modes);
    VkExtent2D _M_choose_swap_extent (const VkSurfaceCapabilitiesKHR& capabilities);
    void _M_create_swap_chain ();
    void _M_create_image_views ();
    void _M_create_graphics_pipeline ();
    static std::vector<char> _M_read_file (const std::string& filename);
    VkShaderModule _M_create_shader_module (const std::vector<char>& code);
    void _M_create_render_pass ();
    void _M_create_framebuffers ();
    void _M_create_command_pool ();
    void _M_create_command_buffers ();
    void _M_draw_frame ();
    void _M_create_sync_objects ();
    void _M_recreate_swap_chain ();
    void _M_cleanup_swap_chain ();
    static void _M_framebuffer_resize_callback (GLFWwindow* window, int width, int height);
    void _M_create_vertex_buffer ();
    uint32_t _M_find_memory_type (uint32_t type_filter, VkMemoryPropertyFlags properties);
    void _M_create_buffer (VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
			   VkBuffer& buffer, VkDeviceMemory& buffer_memory);
    void _M_copy_buffer (VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size);
    void _M_create_index_buffer ();
    void _M_create_descriptor_set_layout ();
    void _M_create_uniform_buffers ();
    void _M_update_uniform_buffer (uint32_t current_image);
    void _M_create_descriptor_pool ();
    void _M_create_descriptor_sets ();
    void _M_create_texture_image ();
    void _M_create_image (uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
			  VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image,
			  VkDeviceMemory& image_memory);
    VkCommandBuffer _M_begin_single_time_commands ();
    void _M_end_single_time_commands (VkCommandBuffer command_buffer);
    void _M_copy_buffer_to_image (VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
    void _M_transition_image_layout (VkImage image, VkFormat format, VkImageLayout old_layout,
				     VkImageLayout new_layout);
    void _M_create_texture_image_view ();
    VkImageView  _M_create_image_view (VkImage image, VkFormat format, VkImageAspectFlags aspect_flags);
    void _M_create_texture_sampler ();
    void _M_create_depth_resources ();
    VkFormat _M_find_supported_format (const std::vector<VkFormat>& candidates, VkImageTiling tiling,
				       VkFormatFeatureFlags features);
    VkFormat _M_find_depth_format ();
    bool _M_has_stencil_component (VkFormat format);
  };
} // namespace app
#endif // APP_VULKAN_HPP
