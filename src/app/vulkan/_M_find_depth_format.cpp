/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

VkFormat
app::vulkan::_M_find_depth_format ()
{
  return _M_find_supported_format ({VK_FORMAT_D32_SFLOAT,
				    VK_FORMAT_D32_SFLOAT_S8_UINT,
				    VK_FORMAT_D24_UNORM_S8_UINT},
                                   VK_IMAGE_TILING_OPTIMAL,
                                   VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}
