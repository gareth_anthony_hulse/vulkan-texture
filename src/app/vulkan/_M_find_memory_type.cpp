/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

uint32_t
app::vulkan::_M_find_memory_type (uint32_t type_filter, VkMemoryPropertyFlags properties)
{
  VkPhysicalDeviceMemoryProperties mem_properties;
  vkGetPhysicalDeviceMemoryProperties (_M_physical_device, &mem_properties);

  for (uint32_t i = 0; i < mem_properties.memoryTypeCount; ++i)
    {
      if ((type_filter & (1 << i)) &&
	  (mem_properties.memoryTypes[i].propertyFlags & properties) == properties)
	{
	  return i;
	}
    }
  throw std::runtime_error ("Failed to find suitable memory type.");
}
