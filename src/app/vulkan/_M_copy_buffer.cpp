/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_copy_buffer (VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size)
{
  VkCommandBuffer command_buffer = _M_begin_single_time_commands ();

  VkBufferCopy copy_region = {};
  copy_region.size = size;
  vkCmdCopyBuffer (command_buffer, src_buffer, dst_buffer, 1, &copy_region);

  _M_end_single_time_commands (command_buffer);
}
