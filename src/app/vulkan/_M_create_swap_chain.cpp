/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_swap_chain ()
{
  swap_chain_support_details swap_chain_support = _M_query_swap_chain_support (_M_physical_device);
  VkSurfaceFormatKHR surface_format = _M_choose_swap_surface_format (swap_chain_support.formats);
  VkPresentModeKHR present_mode = _M_choose_swap_present_mode (swap_chain_support.present_modes);
  VkExtent2D extent = _M_choose_swap_extent (swap_chain_support.capabilities);

  uint32_t image_count = swap_chain_support.capabilities.minImageCount + 1;
  if (swap_chain_support.capabilities.maxImageCount > 0 &&
      image_count > swap_chain_support.capabilities.maxImageCount)
    {
      image_count = swap_chain_support.capabilities.maxImageCount;
    }

  VkSwapchainCreateInfoKHR create_info = {};
  create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
  create_info.surface = _M_surface;
  create_info.minImageCount = image_count;
  create_info.imageFormat = surface_format.format;
  create_info.imageColorSpace = surface_format.colorSpace;
  create_info.imageExtent = extent;
  create_info.imageArrayLayers = 1;
  create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

  queue_family_indices indices = _M_find_queue_families (_M_physical_device);
  uint32_t my_queue_family_indices[] =
    {
     indices.graphics_family.value (),
     indices.present_family.value ()
    };

  if (indices.graphics_family != indices.present_family)
    {
      create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
      create_info.queueFamilyIndexCount = 2;
      create_info.pQueueFamilyIndices = my_queue_family_indices;
    }
  else
    {
      create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }

  create_info.preTransform = swap_chain_support.capabilities.currentTransform;
  create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  create_info.presentMode = present_mode;
  create_info.clipped = VK_TRUE;
  create_info.oldSwapchain = VK_NULL_HANDLE;

  if (vkCreateSwapchainKHR (_M_logical_device, &create_info, nullptr, &_M_swap_chain) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to create swap chain!");
    }

  vkGetSwapchainImagesKHR (_M_logical_device, _M_swap_chain, &image_count, nullptr);
  _M_swap_chain_images.resize (image_count);
  vkGetSwapchainImagesKHR (_M_logical_device, _M_swap_chain, &image_count, _M_swap_chain_images.data ());

  _M_swap_chain_image_format = surface_format.format;
  _M_swap_chain_extent = extent;
}
