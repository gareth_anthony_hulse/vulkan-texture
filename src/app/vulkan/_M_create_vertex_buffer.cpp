/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_vertex_buffer ()
{
  VkDeviceSize buffer_size = sizeof (_M_vertices[0]) * _M_vertices.size ();

  VkBuffer staging_buffer;
  VkDeviceMemory staging_buffer_memory;
  _M_create_buffer (buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, staging_buffer, staging_buffer_memory);

  void* data;
  vkMapMemory (_M_logical_device, staging_buffer_memory, 0, buffer_size, 0, &data);
  memcpy (data, _M_vertices.data (), static_cast<size_t> (buffer_size));
  vkUnmapMemory (_M_logical_device, staging_buffer_memory);

  _M_create_buffer (buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, _M_vertex_buffer, _M_vertex_buffer_memory);

  _M_copy_buffer (staging_buffer, _M_vertex_buffer, buffer_size);

  vkDestroyBuffer (_M_logical_device, staging_buffer, nullptr);
  vkFreeMemory (_M_logical_device, staging_buffer_memory, nullptr);
}
