/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_command_buffers ()
{
  _M_command_buffers.resize (_M_swap_chain_framebuffers.size ());

  VkCommandBufferAllocateInfo alloc_info = {};
  alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  alloc_info.commandPool = _M_command_pool;
  alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  alloc_info.commandBufferCount = static_cast<uint32_t> (_M_command_buffers.size ());

  if (vkAllocateCommandBuffers (_M_logical_device, &alloc_info, _M_command_buffers.data ())
      != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to allocate command buffers.");
    }

  for (size_t i = 0; i < _M_command_buffers.size (); ++i)
    {
      VkCommandBufferBeginInfo begin_info = {};
      begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

      if (vkBeginCommandBuffer (_M_command_buffers[i], &begin_info) != VK_SUCCESS)
	{
	  throw std::runtime_error ("Failed to begin recording command buffer.");
	}

      VkRenderPassBeginInfo render_pass_info = {};
      render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
      render_pass_info.renderPass = _M_render_pass;
      render_pass_info.framebuffer = _M_swap_chain_framebuffers[i];
      render_pass_info.renderArea.offset = {0, 0};
      render_pass_info.renderArea.extent = _M_swap_chain_extent;

      std::array<VkClearValue, 2> clear_values = {};
      clear_values[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
      clear_values[1].depthStencil = {1.0f, 0};

      render_pass_info.clearValueCount = static_cast<uint32_t> (clear_values.size ());
      render_pass_info.pClearValues = clear_values.data ();

      vkCmdBeginRenderPass (_M_command_buffers[i], &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);
      
      vkCmdBindPipeline (_M_command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, _M_graphics_pipeline);

      VkBuffer vertex_buffers[] = {_M_vertex_buffer};
      VkDeviceSize offsets[] = {0};
      vkCmdBindVertexBuffers (_M_command_buffers[i], 0, 1, vertex_buffers, offsets);

      vkCmdBindIndexBuffer (_M_command_buffers[i], _M_index_buffer, 0, VK_INDEX_TYPE_UINT16);

      vkCmdBindDescriptorSets (_M_command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
			       _M_pipeline_layout, 0, 1, &_M_descriptor_sets[i], 0, nullptr);
      
      vkCmdDrawIndexed (_M_command_buffers[i], static_cast<uint32_t> (_M_indicies.size ()), 1, 0, 0, 0);
      
      vkCmdEndRenderPass (_M_command_buffers[i]);

      if (vkEndCommandBuffer (_M_command_buffers[i]) != VK_SUCCESS)
	{
	  throw std::runtime_error ("Failed to record command buffer!");
	}
    }
}
