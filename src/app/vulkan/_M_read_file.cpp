/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

std::vector<char>
app::vulkan::_M_read_file (const std::string& filename)
{
  std::ifstream file (filename, std::ios::ate | std::ios::binary);

  if (!file.is_open ())
    {
      throw std::runtime_error ("Failed to open file.");
    }

  size_t file_size = static_cast<size_t> (file.tellg ());
  std::vector<char> buffer (file_size);

  file.seekg (0);
  file.read (buffer.data (), file_size);
  file.close ();

  return buffer;
}
