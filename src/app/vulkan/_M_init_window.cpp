/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_init_window ()
{
  glfwInit ();
  GLFWmonitor *monitor = glfwGetPrimaryMonitor ();
  const GLFWvidmode *mode = glfwGetVideoMode (monitor);
  _M_delta_ms =std::chrono::duration<double, std::milli> (1.0 / mode->refreshRate * 1000.0);

  glfwWindowHint (GLFW_CLIENT_API, GLFW_NO_API);
  
  _M_window = glfwCreateWindow (mode->width, mode->height, PACKAGE_NAME, monitor, nullptr);
  glfwSetWindowUserPointer (_M_window, this);
  glfwSetFramebufferSizeCallback (_M_window, _M_framebuffer_resize_callback);
}
