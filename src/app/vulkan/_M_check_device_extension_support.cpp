/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

bool
app::vulkan::_M_check_device_extension_support (VkPhysicalDevice physical_device)
{
  uint32_t extension_count;
  vkEnumerateDeviceExtensionProperties (physical_device, nullptr, &extension_count, nullptr);

  std::vector<VkExtensionProperties> available_extensions (extension_count);
  vkEnumerateDeviceExtensionProperties (physical_device, nullptr, &extension_count,
				       available_extensions.data ());

  std::set<std::string> required_extensions (_M_device_extensions.begin (), _M_device_extensions.end ());

  for (const auto& extension : available_extensions)
    {
      required_extensions.erase (extension.extensionName);
    }

  return required_extensions.empty ();
}
