

#include "../vulkan.hpp"

void
app::vulkan::_M_end_single_time_commands (VkCommandBuffer command_buffer)
{
  vkEndCommandBuffer (command_buffer);

  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &command_buffer;

  vkQueueSubmit (_M_graphics_queue, 1, &submit_info, VK_NULL_HANDLE);
  vkQueueWaitIdle (_M_graphics_queue);

  vkFreeCommandBuffers (_M_logical_device, _M_command_pool, 1, &command_buffer);
}
