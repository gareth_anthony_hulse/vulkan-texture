/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

bool
app::vulkan::_M_is_physical_device_suitable (VkPhysicalDevice physical_device)
{
  queue_family_indices indices = _M_find_queue_families (physical_device);

  bool extensions_supported = _M_check_device_extension_support (physical_device);

  bool swap_chain_adequate = false;
  if (extensions_supported)
    {
      swap_chain_support_details swap_chain_support = _M_query_swap_chain_support (physical_device);
      swap_chain_adequate = !swap_chain_support.formats.empty () &&
	!swap_chain_support.present_modes.empty ();
    }

  VkPhysicalDeviceFeatures supported_features;
  vkGetPhysicalDeviceFeatures (physical_device, &supported_features);

  return indices.is_complete () && extensions_supported && swap_chain_adequate &&
    supported_features.samplerAnisotropy;
}
