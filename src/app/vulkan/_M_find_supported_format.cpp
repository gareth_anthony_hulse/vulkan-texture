/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

VkFormat
app::vulkan::_M_find_supported_format (const std::vector<VkFormat>& candidates, VkImageTiling tiling,
				       VkFormatFeatureFlags features)
{
  for (VkFormat format : candidates)
    {
      VkFormatProperties props;
      vkGetPhysicalDeviceFormatProperties (_M_physical_device, format, &props);

      if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
	{
	  return format;
	}
      else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
	{
	  return format;
	}
    }
  throw std::runtime_error ("Failed to find supported format.");
}
