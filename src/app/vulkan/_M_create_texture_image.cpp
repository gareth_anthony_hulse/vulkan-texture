/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define STB_IMAGE_IMPLEMENTATION

#include "../vulkan.hpp"

#include <stb/stb_image.h>

void
app::vulkan::_M_create_texture_image ()
{
  int tex_width, tex_height, tex_channels;
  
  stbi_uc* pixels = stbi_load (PKGDATADIR "/textures/gnu-logo.png", &tex_width, &tex_height,
			       &tex_channels, STBI_rgb_alpha);
  VkDeviceSize image_size = tex_width * tex_height * 4;

  if (!pixels)
    {
      throw std::runtime_error ("Failed to load texture image.");
    }

  VkBuffer staging_buffer;
  VkDeviceMemory staging_buffer_memory;
  _M_create_buffer (image_size, VK_IMAGE_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		    VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, staging_buffer, staging_buffer_memory);

  void* data;
  vkMapMemory (_M_logical_device, staging_buffer_memory, 0, image_size, 0, &data);
  memcpy (data, pixels, static_cast<size_t> (image_size));
  vkUnmapMemory (_M_logical_device, staging_buffer_memory);

  stbi_image_free (pixels);

  _M_create_image (tex_width, tex_height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
		   VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
		   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, _M_texture_image, _M_texture_image_memory);

  _M_transition_image_layout (_M_texture_image,
			      VK_FORMAT_R8G8B8A8_UNORM,
			      VK_IMAGE_LAYOUT_UNDEFINED,
			      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
  
  _M_copy_buffer_to_image (staging_buffer,
			   _M_texture_image,
			   static_cast<uint32_t> (tex_width),
			   static_cast<uint32_t> (tex_height));
  
  _M_transition_image_layout (_M_texture_image,
			      VK_FORMAT_R8G8B8A8_UNORM,
			      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

  vkDestroyBuffer (_M_logical_device, staging_buffer, nullptr);
  vkFreeMemory (_M_logical_device, staging_buffer_memory, nullptr);
}
