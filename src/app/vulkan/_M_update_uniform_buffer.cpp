/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_update_uniform_buffer (uint32_t current_image)
{
  static auto start_time = std::chrono::high_resolution_clock::now ();

  auto current_time = std::chrono::high_resolution_clock::now ();
  float time =
    std::chrono::duration<float, std::chrono::seconds::period> (current_time - start_time).count ();

  uniform_buffer_object ubo = {};
  ubo.model = glm::rotate (glm::mat4 (1.0f),
			   time * glm::radians (90.0f),
			   glm::vec3 (0.0f, 0.0f, 1.0f));
  
  ubo.view = glm::lookAt (glm::vec3 (2.0f, 2.0f, 2.0f),
			  glm::vec3 (0.0f, 0.0f, 0.0f),
			  glm::vec3 (0.0f, 0.0f, 1.0f));
  
  ubo.proj = glm::perspective (glm::radians (45.0f),
			       _M_swap_chain_extent.width /
			       static_cast<float> (_M_swap_chain_extent.height),
			       0.1f,
			       10.0f);

  // Flip image.
  ubo.proj[1][1] *= -1;

  void* data;
  vkMapMemory (_M_logical_device, _M_uniform_buffers_memory[current_image], 0, sizeof (ubo), 0, &data);
  memcpy (data, &ubo, sizeof (ubo));
  vkUnmapMemory (_M_logical_device, _M_uniform_buffers_memory[current_image]);
}
