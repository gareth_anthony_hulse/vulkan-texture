/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_uniform_buffers ()
{
  VkDeviceSize buffer_size = sizeof (app::vulkan::uniform_buffer_object);

  _M_uniform_buffers.resize (_M_swap_chain_images.size ());
  _M_uniform_buffers_memory.resize (_M_swap_chain_images.size ());

  for (size_t i = 0; i < _M_swap_chain_images.size (); ++i)
    {
      _M_create_buffer (buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			_M_uniform_buffers[i], _M_uniform_buffers_memory[i]);
    }
}
