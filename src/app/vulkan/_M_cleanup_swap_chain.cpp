/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_cleanup_swap_chain ()
{
  vkDestroyImageView (_M_logical_device, _M_depth_image_view, nullptr);
  vkDestroyImage (_M_logical_device, _M_depth_image, nullptr);
  vkFreeMemory (_M_logical_device, _M_depth_image_memory, nullptr);
  
  for (auto framebuffer : _M_swap_chain_framebuffers)
    {
      vkDestroyFramebuffer (_M_logical_device, framebuffer, nullptr);
    }
  vkDestroyPipeline (_M_logical_device, _M_graphics_pipeline, nullptr);
  vkDestroyPipelineLayout (_M_logical_device, _M_pipeline_layout, nullptr);
  vkDestroyRenderPass (_M_logical_device, _M_render_pass, nullptr);
  for (auto image_view: _M_swap_chain_image_views)
    {
      vkDestroyImageView (_M_logical_device, image_view, nullptr);
    }
  vkDestroySwapchainKHR (_M_logical_device, _M_swap_chain, nullptr);

  for (size_t i = 0; i < _M_swap_chain_images.size (); ++i)
    {
      vkDestroyBuffer (_M_logical_device, _M_uniform_buffers[i], nullptr);
      vkFreeMemory (_M_logical_device, _M_uniform_buffers_memory[i], nullptr);
    }

  vkDestroyDescriptorPool (_M_logical_device, _M_descriptor_pool, nullptr);
}
