/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::run ()
{
  _M_init_window ();
  _M_init_vulkan ();
  _M_main_loop ();
}
