/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_recreate_swap_chain ()
{
  int width = 0, height = 0;
  while (width == 0 || height == 0)
    {
      glfwGetFramebufferSize (_M_window, &width, &height);
      glfwWaitEvents ();
    }
  
  vkDeviceWaitIdle (_M_logical_device);

  _M_cleanup_swap_chain ();

  _M_create_swap_chain ();
  _M_create_image_views ();
  _M_create_render_pass ();
  _M_create_framebuffers ();
  _M_create_uniform_buffers ();
  _M_create_descriptor_pool ();
  _M_create_command_buffers ();
}
