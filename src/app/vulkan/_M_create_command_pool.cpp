/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_command_pool ()
{
  app::vulkan::queue_family_indices my_queue_family_indices =
    _M_find_queue_families (_M_physical_device);

  VkCommandPoolCreateInfo pool_info = {};
  pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  pool_info.queueFamilyIndex = my_queue_family_indices.graphics_family.value ();

  if (vkCreateCommandPool (_M_logical_device, &pool_info, nullptr, &_M_command_pool) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to create command pool.");
    }
}
