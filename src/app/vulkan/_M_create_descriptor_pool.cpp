/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_descriptor_pool ()
{
  std::array<VkDescriptorPoolSize, 2> pool_sizes = {};
  pool_sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  pool_sizes[0].descriptorCount = static_cast<uint32_t> (_M_swap_chain_images.size ());
  pool_sizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
  pool_sizes[1].descriptorCount = static_cast<uint32_t> (_M_swap_chain_images.size ());

  VkDescriptorPoolCreateInfo pool_info = {};
  pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  pool_info.poolSizeCount = static_cast<uint32_t> (pool_sizes.size ());
  pool_info.pPoolSizes = pool_sizes.data ();
  pool_info.maxSets = static_cast<uint32_t> (_M_swap_chain_images.size ());

  if (vkCreateDescriptorPool (_M_logical_device, &pool_info, nullptr, &_M_descriptor_pool) != VK_SUCCESS)
    {
      throw std::runtime_error("Failed to create descriptor pool.");
    }
}
