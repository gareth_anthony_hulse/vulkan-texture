/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_descriptor_sets ()
{
  std::vector<VkDescriptorSetLayout> layouts (_M_swap_chain_images.size (), _M_descriptor_set_layout);
  VkDescriptorSetAllocateInfo alloc_info = {};
  alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  alloc_info.descriptorPool = _M_descriptor_pool;
  alloc_info.descriptorSetCount = static_cast<uint32_t> (_M_swap_chain_images.size ());
  alloc_info.pSetLayouts = layouts.data ();

  _M_descriptor_sets.resize (_M_swap_chain_images.size ());
  if (vkAllocateDescriptorSets (_M_logical_device, &alloc_info, _M_descriptor_sets.data ())
      != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to allocate descriptor sets.");
    }

  for (size_t i = 0; i < _M_swap_chain_images.size (); ++i)
    {
      VkDescriptorBufferInfo buffer_info = {};
      buffer_info.buffer = _M_uniform_buffers[i];
      buffer_info.offset = 0;
      buffer_info.range = sizeof (app::vulkan::uniform_buffer_object);

      VkDescriptorImageInfo image_info = {};
      image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
      image_info.imageView = _M_texture_image_view;
      image_info.sampler = _M_texture_sampler;

      std::array<VkWriteDescriptorSet, 2> descriptor_writes = {};
      descriptor_writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
      descriptor_writes[0].dstSet = _M_descriptor_sets[i];
      descriptor_writes[0].dstBinding = 0;
      descriptor_writes[0].dstArrayElement = 0;
      descriptor_writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
      descriptor_writes[0].descriptorCount = 1;
      descriptor_writes[0].pBufferInfo = &buffer_info;
      
      descriptor_writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
      descriptor_writes[1].dstSet = _M_descriptor_sets[i];
      descriptor_writes[1].dstBinding = 1;
      descriptor_writes[1].dstArrayElement = 0;
      descriptor_writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
      descriptor_writes[1].descriptorCount = 1;
      descriptor_writes[1].pImageInfo = &image_info;

      vkUpdateDescriptorSets (_M_logical_device, static_cast<uint32_t> (descriptor_writes.size ()),
			      descriptor_writes.data (), 0, nullptr);
    }
}
