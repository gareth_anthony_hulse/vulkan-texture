/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_texture_image_view ()
{
  _M_texture_image_view = _M_create_image_view (_M_texture_image, VK_FORMAT_R8G8B8A8_UNORM,
						VK_IMAGE_ASPECT_COLOR_BIT);
}
