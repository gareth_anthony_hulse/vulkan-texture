/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_image_views ()
{
  _M_swap_chain_image_views.resize (_M_swap_chain_images.size ());

  for (size_t i = 0; i < _M_swap_chain_images.size (); ++i)
    {
      _M_swap_chain_image_views[i] = _M_create_image_view (_M_swap_chain_images[i],
							   _M_swap_chain_image_format,
							   VK_IMAGE_ASPECT_COLOR_BIT);
    }
}
