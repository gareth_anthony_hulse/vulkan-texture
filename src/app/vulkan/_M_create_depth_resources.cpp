/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_depth_resources ()
{
  VkFormat depth_format = _M_find_depth_format ();
  _M_create_image (_M_swap_chain_extent.width, _M_swap_chain_extent.height, depth_format,
		   VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, _M_depth_image, _M_depth_image_memory);
  _M_depth_image_view = _M_create_image_view (_M_depth_image, depth_format, VK_IMAGE_ASPECT_DEPTH_BIT);
}
