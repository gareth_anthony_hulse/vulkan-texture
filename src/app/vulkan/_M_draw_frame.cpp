/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_draw_frame ()
{
  vkWaitForFences (_M_logical_device, 1, &_M_in_flight_fences[_M_current_frame], VK_TRUE, UINT64_MAX);
  
  uint32_t image_index;
  VkResult result = vkAcquireNextImageKHR (_M_logical_device, _M_swap_chain, UINT64_MAX,
					   _M_image_available_semaphores[_M_current_frame],
					   VK_NULL_HANDLE, &image_index);

  if (result == VK_ERROR_OUT_OF_DATE_KHR)
    {
      _M_recreate_swap_chain ();
      return;
    }
  else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
    {
      throw std::runtime_error ("Failed to acquire swap chain image!");
    }

  _M_update_uniform_buffer (image_index);

  if (_M_images_in_flight[image_index] != VK_NULL_HANDLE)
    {
      vkWaitForFences (_M_logical_device, 1, &_M_images_in_flight[image_index], VK_TRUE, UINT64_MAX);
    }
  _M_images_in_flight[image_index] = _M_in_flight_fences[_M_current_frame];

  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  VkSemaphore wait_semaphores[] = {_M_image_available_semaphores[_M_current_frame]};
  VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  submit_info.waitSemaphoreCount = 1;
  submit_info.pWaitSemaphores = wait_semaphores;
  submit_info.pWaitDstStageMask = wait_stages;

  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &_M_command_buffers[image_index];

  VkSemaphore signal_semaphores[] = {_M_render_finished_semaphores[_M_current_frame]};
  submit_info.signalSemaphoreCount = 1;
  submit_info.pSignalSemaphores = signal_semaphores;

  vkResetFences (_M_logical_device, 1, &_M_in_flight_fences[_M_current_frame]);

  if (vkQueueSubmit (_M_graphics_queue, 1, &submit_info, _M_in_flight_fences[_M_current_frame])
      != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to submit draw command buffer.");
    }

  VkPresentInfoKHR present_info = {};
  present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
  present_info.waitSemaphoreCount = 1;
  present_info.pWaitSemaphores = signal_semaphores;
  VkSwapchainKHR swap_chains[] = {_M_swap_chain};
  present_info.swapchainCount = 1;
  present_info.pSwapchains = swap_chains;
  present_info.pImageIndices = &image_index;
  
  // Wait for monitor:
    {
      auto delta_ms_duration = std::chrono::duration_cast<std::chrono::milliseconds> (_M_delta_ms);
      std::this_thread::sleep_for (std::chrono::milliseconds(delta_ms_duration.count ()));
    }
  vkQueuePresentKHR (_M_present_queue, &present_info);
  
  vkQueueWaitIdle (_M_present_queue);

  _M_current_frame = (_M_current_frame + 1) % _M_max_frames_in_flight;
}
