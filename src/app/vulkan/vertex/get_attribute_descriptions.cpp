/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../../vulkan.hpp"

std::array<VkVertexInputAttributeDescription, 3>
app::vulkan::vertex::get_attribute_descriptions ()
{
  std::array<VkVertexInputAttributeDescription, 3> attribute_descriptions = {};
  attribute_descriptions[0].binding = 0;
  attribute_descriptions[0].location = 0;
  attribute_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
  attribute_descriptions[0].offset = offsetof (app::vulkan::vertex, pos);

  attribute_descriptions[1].binding = 0;
  attribute_descriptions[1].location = 1;
  attribute_descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
  attribute_descriptions[1].offset = offsetof (app::vulkan::vertex, color);

  attribute_descriptions[2].binding = 0;
  attribute_descriptions[2].location = 2;
  attribute_descriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;
  attribute_descriptions[2].offset = offsetof (app::vulkan::vertex, tex_coord);
  
  return attribute_descriptions;
}
