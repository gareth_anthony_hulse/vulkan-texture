/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

VkPresentModeKHR
app::vulkan::_M_choose_swap_present_mode (const std::vector<VkPresentModeKHR>& available_present_modes)
{
  for (const auto& available_present_mode : available_present_modes)
    {
      // With Wayland, there should be no screen tearing.
      if (available_present_mode == VK_PRESENT_MODE_IMMEDIATE_KHR &&
	  getenv ("XDG_SESSION_TYPE") == _M_target_session_type)
	{
	  return available_present_mode;
	}
      if (available_present_mode == VK_PRESENT_MODE_MAILBOX_KHR)
	{
	  return available_present_mode;
	}
    }
  return VK_PRESENT_MODE_FIFO_KHR;
}
