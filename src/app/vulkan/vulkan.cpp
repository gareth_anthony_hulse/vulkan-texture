/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

app::vulkan::~vulkan ()
{
  _M_cleanup_swap_chain ();

  vkDestroySampler (_M_logical_device, _M_texture_sampler, nullptr);
  vkDestroyImageView (_M_logical_device, _M_texture_image_view, nullptr);

  vkDestroyImage (_M_logical_device, _M_texture_image, nullptr);
  vkFreeMemory (_M_logical_device, _M_texture_image_memory, nullptr);

  vkDestroyDescriptorSetLayout (_M_logical_device, _M_descriptor_set_layout, nullptr);

  vkDestroyBuffer (_M_logical_device, _M_index_buffer, nullptr);
  vkFreeMemory (_M_logical_device, _M_index_buffer_memory, nullptr); 
   
  vkDestroyBuffer (_M_logical_device, _M_vertex_buffer, nullptr);
  vkFreeMemory (_M_logical_device, _M_vertex_buffer_memory, nullptr);
  
  for (uint8_t i = 0; i < _M_max_frames_in_flight; ++i)
    {
      vkDestroySemaphore (_M_logical_device, _M_render_finished_semaphores[i], nullptr);
      vkDestroySemaphore (_M_logical_device, _M_image_available_semaphores[i], nullptr);
      vkDestroyFence (_M_logical_device, _M_in_flight_fences[i], nullptr);
    }
  
  vkDestroyCommandPool (_M_logical_device, _M_command_pool, nullptr);
  
  vkDestroyDevice (_M_logical_device, nullptr);
#ifndef NDEBUG
  _M_destroy_debug_messenger (_M_instance, _M_debug_messenger, nullptr);
#endif // NDEBUG
  
  vkDestroySurfaceKHR (_M_instance, _M_surface, nullptr);
  vkDestroyInstance (_M_instance, nullptr);
  
  glfwDestroyWindow (_M_window);
  
  glfwTerminate ();
}

