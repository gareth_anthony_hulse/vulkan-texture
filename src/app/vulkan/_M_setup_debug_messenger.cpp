/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef NDEBUG
#include "../vulkan.hpp"

void
app::vulkan::_M_setup_debug_messenger ()
{
  VkDebugUtilsMessengerCreateInfoEXT create_info;
  _M_populate_debug_messenger_create_info (create_info);
  
  if (_M_create_debug_messenger (_M_instance, &create_info, nullptr, &_M_debug_messenger) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to setup debug messenger!");
    }
}
#endif //NDEBUG
