/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef NDEBUG
#include "../vulkan.hpp"

void
app::vulkan::_M_destroy_debug_messenger (VkInstance instance,
					 VkDebugUtilsMessengerEXT debug_messenger,
					 const VkAllocationCallbacks* allocator)
{
  auto destroy_debug_messenger = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>
    (vkGetInstanceProcAddr (instance, "vkDestroyDebugUtilsMessengerEXT"));
  if (destroy_debug_messenger != nullptr)
    {
      destroy_debug_messenger (instance, debug_messenger, allocator);
    }
}

#endif // NDEBUG
