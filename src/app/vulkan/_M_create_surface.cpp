/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_surface ()
{
  if (glfwCreateWindowSurface (_M_instance, _M_window, nullptr, &_M_surface) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to create window surface!");
    }
}
