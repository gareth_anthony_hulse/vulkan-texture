/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_framebuffers ()
{
  _M_swap_chain_framebuffers.resize (_M_swap_chain_image_views.size ());

  for (size_t i = 0; i < _M_swap_chain_image_views.size (); ++i)
    {
      std::array<VkImageView, 2> attachments = {_M_swap_chain_image_views[i], _M_depth_image_view};

        VkFramebufferCreateInfo framebuffer_info = {};
	framebuffer_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebuffer_info.renderPass = _M_render_pass;
	framebuffer_info.attachmentCount = static_cast<uint32_t> (attachments.size ());
	framebuffer_info.pAttachments = attachments.data ();
	framebuffer_info.width = _M_swap_chain_extent.width;
	framebuffer_info.height = _M_swap_chain_extent.height;
	framebuffer_info.layers = 1;

	if (vkCreateFramebuffer (_M_logical_device, &framebuffer_info, nullptr,
				 &_M_swap_chain_framebuffers[i]) != VK_SUCCESS)
	  {
	    throw std::runtime_error ("Failed to create framebuffer.");
	  }
    };
}
