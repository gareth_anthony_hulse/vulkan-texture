/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_init_vulkan ()
{
  _M_create_instance ();
#ifndef NDEBUG
  _M_setup_debug_messenger ();
#endif //NDEBUG
  _M_create_surface ();
  _M_pick_physical_device ();
  _M_create_logical_device ();
  _M_create_swap_chain ();
  _M_create_image_views ();
  _M_create_render_pass ();
  _M_create_descriptor_set_layout ();
  _M_create_graphics_pipeline ();
  _M_create_depth_resources ();
  _M_create_framebuffers ();
  _M_create_command_pool ();
  _M_create_texture_image ();
  _M_create_texture_image_view ();
  _M_create_texture_sampler ();
  _M_create_vertex_buffer ();
  _M_create_index_buffer ();
  _M_create_uniform_buffers ();
  _M_create_descriptor_pool ();
  _M_create_descriptor_sets ();
  _M_create_command_buffers ();
  _M_create_sync_objects ();
}
