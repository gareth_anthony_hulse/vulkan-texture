/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_sync_objects ()
{
  _M_image_available_semaphores.resize (_M_max_frames_in_flight);
  _M_render_finished_semaphores.resize (_M_max_frames_in_flight);
  _M_in_flight_fences.resize (_M_max_frames_in_flight);
  _M_images_in_flight.resize (_M_swap_chain_images.size (), VK_NULL_HANDLE);
  
  VkSemaphoreCreateInfo semaphore_info = {};
  semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

  VkFenceCreateInfo fence_info = {};
  fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

  for (size_t i = 0; i < _M_max_frames_in_flight; ++i)
    {
      if (vkCreateSemaphore (_M_logical_device,
			     &semaphore_info,
			     nullptr,
			     &_M_image_available_semaphores[i]) != VK_SUCCESS ||
	  vkCreateSemaphore (_M_logical_device,
			     &semaphore_info,
			     nullptr,
			     &_M_render_finished_semaphores[i]) != VK_SUCCESS ||
	  vkCreateFence (_M_logical_device,
			 &fence_info,
			 nullptr,
			 &_M_in_flight_fences[i]) != VK_SUCCESS)
	{
	  throw std::runtime_error ("Failed to create synchronization objects for a frame.");
	}
    }
}
