/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

bool
app::vulkan::_M_has_stencil_component (VkFormat format)
{
  return format == VK_FORMAT_D24_UNORM_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}
