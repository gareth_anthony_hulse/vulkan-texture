/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

app::vulkan::queue_family_indices
app::vulkan::_M_find_queue_families (VkPhysicalDevice physical_device)
{
  queue_family_indices indices;

  uint32_t queue_family_count = 0;
  vkGetPhysicalDeviceQueueFamilyProperties (physical_device, &queue_family_count, nullptr);
  
  std::vector<VkQueueFamilyProperties> queue_families (queue_family_count);
  vkGetPhysicalDeviceQueueFamilyProperties (physical_device,
					    &queue_family_count,
					    queue_families.data ());

  int i = 0;
  for (const auto& queue_family : queue_families)
    {
      if (queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT)
	{
	  indices.graphics_family = i;
	}

      VkBool32 present_support = false;
      vkGetPhysicalDeviceSurfaceSupportKHR (physical_device, i, _M_surface, &present_support);

      if (present_support)
	{
	  indices.present_family = i;
	}

      if (indices.is_complete ())
	{
	  break;
	}
      
      ++i;
    }
  
  return indices;
}
