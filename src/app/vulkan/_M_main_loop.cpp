/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_main_loop ()
{
  // FPS Counter:
  /*
  auto timer = std::chrono::steady_clock();
  auto last_time = timer.now();
  uint64_t frame_counter = 0;
  uint64_t fps = 0;
  */
  
  while (!glfwWindowShouldClose (_M_window))
    { 
      glfwPollEvents ();
      _M_draw_frame ();

      // FPS Counter:
      /*
      ++frame_counter;
      if( last_time + std::chrono::seconds (1) < timer.now ())
	{
	  last_time = timer.now ();
	  fps = frame_counter;
	  frame_counter = 0;
	  fprintf (stdout, "FPS: %lu\n", fps);
	}
      */
    }

  vkDeviceWaitIdle (_M_logical_device);
}
