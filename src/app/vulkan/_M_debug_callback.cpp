/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef NDEBUG
#include "../vulkan.hpp"

VKAPI_ATTR VkBool32 VKAPI_CALL
app::vulkan::_M_debug_callback (VkDebugUtilsMessageSeverityFlagBitsEXT /*message_severity*/,
				VkDebugUtilsMessageTypeFlagsEXT /*message_type*/,
				const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
				void* /*user_data*/)
{
  fprintf (stderr, "Validation layer: %s\n", callback_data->pMessage);

  return VK_FALSE;
}
#endif // NDEBUG
