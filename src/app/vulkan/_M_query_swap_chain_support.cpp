/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

app::vulkan::swap_chain_support_details
app::vulkan::_M_query_swap_chain_support (VkPhysicalDevice physical_device)
{
  swap_chain_support_details details;

  vkGetPhysicalDeviceSurfaceCapabilitiesKHR (physical_device, _M_surface, &details.capabilities);

  uint32_t format_count;
  vkGetPhysicalDeviceSurfaceFormatsKHR (physical_device, _M_surface, &format_count, nullptr);

  if (format_count != 0)
    {
      details.formats.resize (format_count);
      vkGetPhysicalDeviceSurfaceFormatsKHR (physical_device, _M_surface, &format_count,
					    details.formats.data ());
    }

  uint32_t present_mode_count;
  vkGetPhysicalDeviceSurfacePresentModesKHR (physical_device, _M_surface, &present_mode_count, nullptr);

  if (present_mode_count != 0)
    {
      details.present_modes.resize (present_mode_count);
        vkGetPhysicalDeviceSurfacePresentModesKHR (physical_device, _M_surface, &present_mode_count,
					     details.present_modes.data ());
    }

  return details;
}
