
#include "../vulkan.hpp"

void
app::vulkan::_M_copy_buffer_to_image (VkBuffer buffer, VkImage image, uint32_t width, uint32_t height)
{
  VkCommandBuffer command_buffer = _M_begin_single_time_commands ();

  VkBufferImageCopy region = {};
  region.bufferOffset = 0;
  region.bufferRowLength = 0;
  region.bufferImageHeight = 0;

  region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  region.imageSubresource.mipLevel = 0;
  region.imageSubresource.baseArrayLayer = 0;
  region.imageSubresource.layerCount = 1;

  region.imageOffset = {0, 0, 0};
  region.imageExtent = {width, height, 1};

  vkCmdCopyBufferToImage (command_buffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
			  &region);
  
  _M_end_single_time_commands (command_buffer);
}
