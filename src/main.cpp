/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "app/vulkan.hpp"
#include <stdexcept>
#include <cstdio>

int
main ()
{
  try
    {
       app::vulkan vulkan_app;
       vulkan_app.run ();
    }
  catch (const std::exception &err)
    {
      fprintf (stderr, "%s\n", err.what ());
      return EXIT_FAILURE;      
    }
  
  return EXIT_SUCCESS;
}
